from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index, addKegiatan, addPeserta
from .models import Person, Kegiatan

# Create your tests here.
class UrlsTest(TestCase):
	def setUp(self):
		self.kegiatan = Kegiatan.objects.create(name="percobaan")

	def test_url_kegiatan_is_exist(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code, 200)

	def test_url_kegiatan_use_right_func(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, index)

	def test_url_addKegiatan_is_exist(self):
		response = Client().get('/story6/addKegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_url_addKegiatan_use_right_func(self):
		found = resolve('/story6/addKegiatan/')
		self.assertEqual(found.func, addKegiatan)

	def test_POST_addKegiatan(self):
		response = Client().post('/story6/addKegiatan/', {'name': 'asikjos'}, follow=True)
		self.assertEqual(response.status_code, 200)

	def test_url_addPeserta_is_exist(self):
		pk_kegiatan = str(self.kegiatan.pk)
		response = Client().get('/story6/addPeserta/' + pk_kegiatan + '/')
		self.assertEqual(response.status_code, 200)

	def test_url_addPeserta_use_right_func(self):
		pk_kegiatan = str(self.kegiatan.pk)
		found = resolve('/story6/addPeserta/' + pk_kegiatan + '/')
		self.assertEqual(found.func, addPeserta)

	def test_POST_addPeserta(self):
		pk_kegiatan = str(self.kegiatan.pk)
		url = '/story6/addPeserta/' + pk_kegiatan + '/'
		response = Client().post(url, {'name': 'asikjos'}, follow=True)
		self.assertEqual(response.status_code, 200)


class ModelsTest(TestCase):

	def setUp(self):
		self.kegiatan = Kegiatan.objects.create(name="Kegiatan1")
		self.peserta = Person.objects.create(name="Joni Mustopa")

	def test_kegiatan_dan_peserta_added(self):
		self.assertEqual(Kegiatan.objects.count(), 1)
		self.assertEqual(Person.objects.count(), 1)

	def test_str_kegiatan_dan_peserta(self):
		self.assertEqual(str(self.kegiatan), "Kegiatan1")
		self.assertEqual(str(self.peserta), "Joni Mustopa")