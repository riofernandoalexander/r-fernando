from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import index

# Create your tests here.
class TestStoryTujuh(TestCase):
	def test_url_index_story7_is_exist(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)

	def test_index_use_right_template_and_right_title_in_template(self):
		request = HttpRequest()
		response = index(request)
		context = Client().get('/story7/')
		html_response = response.content.decode('utf8')
		self.assertTemplateUsed(context, 'story7/index.html')
		self.assertIn('Implementation of JavaScript and Jquery', html_response)
