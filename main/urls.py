from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('qna/', views.qna, name='qna'),
    path('oldwebsite/', views.oldweb, name='oldweb'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('matkuls/', views.showmatkul, name="showmatkul"),
    path('delete_matkul/<str:pk>/', views.deleteMatkul, name="deletematkul")
]
