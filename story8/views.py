from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
	return render(request, 'story8/index.html')

def json_data(request):
	url = 	'https://www.googleapis.com/books/v1/volumes?q='+request.GET['q']
	link = requests.get(url)
	json_text = link.text
	data = json.loads(json_text)
	return JsonResponse(data, safe=False)
