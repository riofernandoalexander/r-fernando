from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import index

# Create your tests here.
class UnitTestStory8(TestCase):
	def test_url_index_story8_is_exist(self):
		response = Client().get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_index_use_right_template(self):
		request = HttpRequest()
		response = index(request)
		context = Client().get('/story8/')
		html_response = response.content.decode('utf8')
		self.assertIn('Search', html_response)
		self.assertIn('Masukkan kata kunci...', html_response)
		self.assertTemplateUsed(context, 'story8/index.html')
