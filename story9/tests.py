from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index, signup, login
from django.contrib.auth.models import User

# Create your tests here.
class UrlsTests(TestCase):
	def test_url_index_is_exist(self):
		response = Client().get('/story9/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'story9/index.html')

	def test_url_signup_is_exist(self):
		response = Client().get('/story9/signup/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'story9/signup.html')

	def test_url_login_is_exist(self):
		response = Client().get('/story9/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'story9/login.html')

class ModelsTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user('uname', password='pass', is_staff=True)
		self.signup_response_success = Client().post('/story9/signup/',{
			'username':'jubaedi', 'email':'jubaedi@gmail.com',
			'password1' : 'bismillahh', 'password2': 'bismillahh'
			})
		self.signup_response_fail = Client().post('/story9/signup/',{
			'username':'jubaedi', 'email':'jubaedigmail.com',
			'password1' : 'bismillahh', 'password2': 'bismillahhh'
			})
		self.login_response_success = Client().post('/story9/login/', {
			'username':'uname', 'password':'pass'
			})
		self.login_response_fail = Client().post('/story9/login/', {
			'username':'uname', 'password':'passSalah'
			})
	def test_user_ada(self):
		self.assertEqual(User.objects.count(), 2)

	def test_signup(self):
		self.assertEqual(self.signup_response_success.status_code, 302)
		self.assertEqual(self.signup_response_fail.status_code, 200)

	def test_login(self):
		self.assertEqual(self.login_response_success.status_code, 302)
		self.assertEqual(self.login_response_fail.status_code, 200)


